﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalculadoraPage.aspx.cs" Inherits="CalculadoraWebForms.CalculadoraPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calculadora</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>CALCULADORA</h1>
        <div>
            <label for="txtNumero1">Número 1:</label>
            <asp:TextBox ID="txtNumero1" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="regexNumeros" runat="server"
                ControlToValidate="txtNumero1"
                ValidationExpression="^\d+(\.\d+)?$"
                ErrorMessage="Digite apenas números. Décimais devem ser sepados por ponto(.)."
                ForeColor="Red">
            </asp:RegularExpressionValidator>
        </div>
        <div>
            <label for="ddlOperacao">Operação:</label>
            <asp:DropDownList ID="ddlOperacao" runat="server">
                <asp:ListItem Text="" Value="0" />
                <asp:ListItem Text="+" Value="soma" />
                <asp:ListItem Text="-" Value="subtracao" />
                <asp:ListItem Text="*" Value="multiplicacao" />
                <asp:ListItem Text="÷" Value="divisao" />
            </asp:DropDownList>
        </div>
        <div>
            <label for="txtNumero2">Número 2:</label>
            <asp:TextBox ID="txtNumero2" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="regexNumeros2" runat="server"
                ControlToValidate="txtNumero2"
                ValidationExpression="^\d+(\.\d+)?$"
                ErrorMessage="Digite apenas números. Décimais devem ser sepados por ponto(.)."
                ForeColor="Red">
            </asp:RegularExpressionValidator>
        </div>
        <div>
            <asp:Button ID="btnCalcular" runat="server" Text="Calcular" OnClick="btnCalcular_Click" />
        </div>
        <div>
            <label for="lblResultado">Resultado:</label>
            <asp:Label ID="lblResultado" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
