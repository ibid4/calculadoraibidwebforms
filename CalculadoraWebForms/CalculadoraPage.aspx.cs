﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CalculadoraWebForms
{
    public partial class CalculadoraPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNumero1.Text) || string.IsNullOrWhiteSpace(txtNumero2.Text))
            {
                lblResultado.Text = "Por favor, insira valores";
                return;
            }

            double numero1 = Convert.ToDouble(txtNumero1.Text, CultureInfo.InvariantCulture);
            double numero2 = Convert.ToDouble(txtNumero2.Text, CultureInfo.InvariantCulture);
            string operacao = ddlOperacao.SelectedValue;
            double resultado = 0;

            switch (operacao)
            {
                case "soma":
                    resultado = numero1 + numero2;
                    break;
                case "subtracao":
                    resultado = numero1 - numero2;
                    break;
                case "multiplicacao":
                    resultado = numero1 * numero2;
                    break;

                case "divisao":
                    if (numero2 != 0)
                    {
                        resultado = numero1 / numero2;
                    }
                    else
                    {
                        lblResultado.Text = "Não é possível dividir por zero.";
                        return;
                    }
                    break;

                default:
                    lblResultado.Text = "Selecione uma operação";
                    return;
            }

            lblResultado.Text = resultado.ToString();
        }
    }
}